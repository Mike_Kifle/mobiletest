import time

from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

desired_caps = dict(
    deviceName='Android',
    platformName='Android',
    automationName="UiAutomator2",
    appPackage="com.amazon.mShop.android.shopping",
    appActivity="com.amazon.mShop.home.HomeActivity",
    udid="emulator-5554",
    noReset=True
)

# desired_cap['app'] = str(Path().absolute().parent)+'\\app\\amazon.apk'

capabilities_options = UiAutomator2Options().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723', options=capabilities_options)
driver.implicitly_wait(20)

'''driver.find_element(By.ID, "com.android.permissioncontroller:id/permission_allow_button").click()
driver.find_element(AppiumBy.ACCESSIBILITY_ID,'Select English').click()
driver.find_element(By.ID, 'in.amazon.mShop.android.shopping:id/continue_button').click()
driver.find_element_by_android_uiautomator('new UiSelector().text("Skip sign in")').click()
# driver.find_element_by_id('in.amazon.mShop.android.shopping:id/skip_sign_in_button').click()
driver.find_element(AppiumBy.ID, 'in.amazon.mShop.android.shopping:id/rs_search_src_text').click()
'''
Search = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((AppiumBy.ID,"com.amazon.mShop.android.shopping:id/rs_search_src_text")
))
Search.send_keys('Nike mens Air Jordan 3')
driver.press_keycode(66)
time.sleep(10)
el = driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                         'new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView'
                         '(new UiSelector().textContains(''"Nike mens Air Jordan 3 Basketball").instance(0))')
action = TouchAction(driver)
action.long_press(el).perform()
driver.swipe(514, 600, 514, 200, 1000)
driver.swipe(514, 600, 514, 200, 1000)
driver.swipe(514, 600, 514, 200, 1000)
driver.swipe(514, 600, 514, 200, 1000)

driver.swipe(514, 500, 514, 800, 1000)
driver.swipe(514, 500, 514, 800, 1000)
driver.swipe(514, 500, 514, 800, 1000)
driver.swipe(514, 500, 514, 800, 1000)
time.sleep(10)
driver.quit()
