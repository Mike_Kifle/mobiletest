import time

import appium
from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from selenium.webdriver.common.by import By
from appium.options.android import UiAutomator2Options

desired_caps = dict(
    deviceName='emulator-5554',
    platformName='Android',
    browserName='Chrome',
    automationName="UiAutomator2"

)

capabilities_options = UiAutomator2Options().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723',options=capabilities_options)

driver.get("http://google.com")
driver.implicitly_wait(10)
driver.find_element(By.XPATH,"//*[@name='q']").send_keys("Hello Appium !!!")
#driver.find_element(By.NAME("q").send_keys("Hello Appium !!!"))
print(driver.title)
time.sleep(2)
driver.quit()