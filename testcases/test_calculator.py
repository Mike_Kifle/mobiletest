import time

from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy


desired_caps = dict(
    deviceName='Android',
    platformName='Android',
    automationName="UiAutomator2",
    appPackage="com.google.android.calculator",
    appActivity="com.android.calculator2.Calculator",
    udid="emulator-5554",
    noReset=True
)

capabilities_options = UiAutomator2Options().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723',options=capabilities_options)
driver.implicitly_wait(20)
driver.find_element(AppiumBy.ID, "com.google.android.calculator:id/digit_2").click()
print("Pressing 2")
driver.find_element(AppiumBy.ACCESSIBILITY_ID,'plus').click()
driver.find_element(AppiumBy.ID,'com.google.android.calculator:id/digit_4').click()
print("Pressing 4")
driver.find_element(AppiumBy.ID,'com.google.android.calculator:id/eq').click()
time.sleep(10)
result = driver.find_element(AppiumBy.ID,'com.google.android.calculator:id/result_final').text
print(result)
assert int(result) == 6
time.sleep(2)
driver.quit()
