import time

from appium import webdriver
from appium.options.android import UiAutomator2Options
from appium.webdriver.common.appiumby import AppiumBy
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

desired_caps = dict(
    deviceName='emulator-5554',
    platformName='Android',
    browserName='Chrome',
    automationName="UiAutomator2"
)

capabilities_options = UiAutomator2Options().load_capabilities(desired_caps)
driver = webdriver.Remote('http://127.0.0.1:4723',options=capabilities_options)
driver.implicitly_wait(10)
driver.get("http://google.com")
print(driver.title)
inputA = WebDriverWait(driver, 30).until(
    EC.element_to_be_clickable((AppiumBy.XPATH,"//*[@name='q']"))
)
inputA.send_keys("Hello Appium !!!")
results = driver.find_elements(AppiumBy.XPATH,"//*[contains(text(),'Appium')]")
print(len(results))
for listed in results:
    print(listed.text)
    if listed.text == 'appium':
        print("Text found")
        break
driver.quit()